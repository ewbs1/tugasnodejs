const fs = require('fs'); 
  
// Initiating asyn function 
async function stop(path) { 
  
    // Creating and initiating directory's 
    // underlying resource handle 
    const dir = await fs.promises.opendir(path); 
  
    console.log("All the dirent before operation :- "); 
  
    for (var i = 0; i <= 2; i++) { 
        console.log(((dir.readSync())).name); 
    } 
  
    // Getting the path of the directory 
    // by using path() method 
    const pathval = dir.path; 
  
    console.log("All the dirent after operation :- "); 
  
    for (var i = 0; i <= 2; i++) { 
        console.log(((dir.readSync())).name); 
    } 
  
    // Display the result 
    console.log("\nPath :- " + pathval); 
} 
  
// Catching error 
stop('./').catch(console.error);